'use strict';

/**
 * @ngdoc function
 * @name angularjsProjelerApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularjsProjelerApp
 */
angular.module('angularjsProjelerApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
