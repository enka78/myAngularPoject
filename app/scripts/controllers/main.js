'use strict';

/**
 * @ngdoc function
 * @name angularjsProjelerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularjsProjelerApp
 */
angular.module('angularjsProjelerApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
